import store from "../store/index";
import { addArticle } from "../store/actions";

window.store = store;
window.addArticle = addArticle;