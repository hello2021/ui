import React, { Component } from 'react';

import {connect} from 'react-redux';

import './Header.scss';

class Header extends React.Component {
    render() {
        return (
            <div className="header">
                <button type="button" className="login" onClick={this.loginClick}>Login</button>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        authenticated: state.authenticated,
        loggedUserDetails: state.loggedUserDetails
    };
}

export default connect(mapStateToProps)(Header);