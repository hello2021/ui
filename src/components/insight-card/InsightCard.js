import React, { Component } from 'react';

import './InsightCard.scss';


class InsightCard extends React.Component {
    constructor() {
        super();
    }

    renderSwitch(type) {
        switch(type) {
            case 'success':
              return <img src={require('../../assets/images/thumbs-up.PNG')} /> ;
            case 'error':
              return <img src={require('../../assets/images/error.PNG')} /> ;
            case 'warning':
              return <img src={require('../../assets/images/warning.PNG')} /> ;
            default:
              return <img src={require('../../assets/images/thumbs-up.PNG')} />;
          }
    }

    render() {
        return (
            <div className={"card " + this.props.type}>
                <div className="card-title">
                    {this.renderSwitch(this.props.type)}
                    {this.props.title}
                </div>
                <div className="card-description">
                    {
                        this.props.description.map(desc => {
                            if(desc.highlight) {
                                return <a class="highlight">{desc.text}</a>
                            } else {
                                return <span>{desc.text}</span>
                            }
                        })
                    }
                </div>
            </div>
        );
    }
}

export default InsightCard;
