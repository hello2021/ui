import React, { Component } from 'react';

import { Link } from 'react-router-dom';

import './Navigation.scss';


class Navigation extends React.Component {
    constructor() {
        super();
    }

    onNavItemClick(item) {
        if(item.children) {
            item.showChildren = !item.showChildren;
        } else {
            this.props.onMenuClick(item.text);
        }
    }

    render() {
        return (
                <div className="nav-container">
                    <img width="100%" src={require('../../assets/images/logo.PNG')} />
                    <div className="nav-items">
                        {this.props.items.map((item, index) => {
                            return (<div className="nav-item" key={'item'+index}>
                                <a onClick={()=> {this.onNavItemClick(item)}}>
                                    <img src={require('../../assets/images/insights.PNG')} />
                                    <div className="nav-item-text">{item.text}</div>
                                    <br/>
                                    
                                </a>
                                <div className="child-items">
                                    {
                                        (item.showChildren && item.children) ? (
                                            item.children.map((child, childIndex) => {
                                                return <div className="nav-item" key={'child' + index + '-' +childIndex}>
                                                    <a onClick={()=> {this.onNavItemClick(child)}}>
                                                        <div className="nav-item-text">{child.text}</div>
                                                    </a>
                                                </div>
                                            })) : ''
                                    }
                                </div>
                            </div>)
                        })}
                    </div>
                </div>
        )
    }
}

export default Navigation;