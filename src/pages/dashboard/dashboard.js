import React, { Component } from 'react';

import { Route, BrowserRouter as Router, Switch } from 'react-router-dom';

import Navigation from '../../components/navigation/Navigation.js';

import { createBrowserHistory } from 'history';

import Header from '../../components/header/Header';
import InsightsPage from './contents/InsightsPage.js';
import Coding from './contents/Coding.js'


class Dashboard extends React.Component {

    history = createBrowserHistory();

    navItems = [
        {
            text: 'Insights',
            route: '/insights',
            imgSrc: 'insights.png'
        },
        {
            text: 'Coding',
            route: '/coding',
            imgSrc: 'insights.png'
        },
        {
            text: 'Languages',
            route: '/languages',
            imgSrc: 'insights.png'
        },
        {
            text: 'Commits',
            route: '/commits',
            imgSrc: 'insights.png'
        },
        {
            text: 'Process',
            route: '/process',
            imgSrc: 'insights.png'
        },
        {
            text: 'Projects',
            imgSrc: 'insights.png',
            children: [
                {
                    text: 'Project One',
                },
                {
                    text: 'Project Two',
                }
            ]
        },
        {
            text: 'Pull requests',
            route: '/pull-requests',
            imgSrc: 'insights.png'
        },
        {
            text: 'Graphs',
            showChildren: false,
            children: [
                {
                    text: 'Sankey',
                    route: '/sankey'
                },
                {
                    text: 'Bar',
                    route: '/bar'
                },
                {
                    text: 'Bubble',
                    route: '/bubble'
                },
                {
                    text: 'Line',
                    route: '/line'
                }
            ]
        }
    ]

    constructor(props) {
        super();
        this.state = {
            path: 'insights'
        }
        this.onMenuClick = this.onMenuClick.bind(this);
    }

    onMenuClick = (path) => {
        this.setState({path: path})
    }

    renderComponents(path) {
        switch(path) {
            case 'Insights':
                return <InsightsPage />;
            case 'Coding':
                return <Coding />;
        }
    }



    render() {
        return (
            <div className="dashboard">
                <Navigation items={this.navItems} onMenuClick={this.onMenuClick} />
                <div className="main">
                    <Header />
                    {/* <Graphs chart={this.state.chart}/> */}
                    {/* <Router history={this.history}>
                        <Switch>
                            <Route path="/insights" />
                            <Route path="/coding" component={InsightsPage} />
                        </Switch>
                    </Router> */}
                    <div className="content">
                    {
                        this.renderComponents(this.state.path)
                    }
                    </div>
                </div>
            </div>
        )
    }
}

export default Dashboard;