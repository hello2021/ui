import React, { Component } from 'react';
import {connect} from 'react-redux';

import InsightsPage from './InsightsPage';

import Bar from '../../graphs/Bar';



class Coding extends React.Component {
    constructor() {
        super();
    }


    render() {
        return (
            <div className="coding-block">
                
                <Bar />
                <InsightsPage />
            </div>
        )
    }
}



export default Coding;