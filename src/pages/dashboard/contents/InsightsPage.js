import React, { Component } from 'react';
import {connect} from 'react-redux';

import InsightCard from '../../../components/insight-card/InsightCard';

import { getInsights } from "../../../store/actions";


class InsightsPage extends React.Component {
    constructor() {
        super();
    }

    componentDidMount() {
        this.props.getInsights();
    }

    render() {
        return (
            <div className="insights-page">
                {this.props.insights.map(insight => <InsightCard title={insight.title} description={insight.description} type={insight.type} />)}
            </div>
        )
    }
}


function mapStateToProps(state) {
    return {
        insights: state.insights
    };
}

export default connect(mapStateToProps, { getInsights })(InsightsPage);