import React, { Component } from 'react';

import Sankey from './Sankey.js';
import Bar from './Bar.js';
import Line from './Line.js';
import Bubble from './Bubble.js';



class Graphs extends React.Component {
    constructor() {
        super();
    }

    renderSwitch() {
        switch(this.props.chart) {
            case 'Sankey':
                return <Sankey />;
            case 'Bar':
                return <Bar />;
            case 'Line':
                return <Line />;
            case 'Bubble':
                return <Bubble />;
        }
    }

    render() {
        return (
            <div>
                {this.renderSwitch()}
            </div>
        )
    }
}

export default Graphs;