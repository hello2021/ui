import React from 'react';

import Chart from 'react-google-charts';

const data = [
    ['City', '2000 Population', '2010 Population', '2020 Population'],
    ['New York City, NY', 8175000, 8008000, 3008000],
    ['Los Angeles, CA', 3792000, 3694000, 4694000],
    ['Chicago, IL', 2695000, 2896000, 2896000],
    ['Houston, TX', 2099000, 1953000, 5953000],
    ['Philadelphia, PA', 1526000, 1517000, 1417000],
    ['Michigan, NY', 4175000, 8008000, 2008000],
    ['Detroit, CA', 3792000, 3694000, 4694000],
    ['Dallas, IL', 6695000, 2896000, 5896000],
    ['New Jersy, TX', 2099000, 5953000, 6953000],
    ['Washington, PA', 1526000, 4517000, 7517000],
];

class Bar extends React.Component {
    
    constructor() {
        super();
        this.state = {
            supplyData: data,
            show2020: true,
            show2010: true,
            show2000: true
        };
    }

    modifyData(year) {
        // if(year === 2020) {
        //     this.setState({show2020: !this.state.show2020});
        // } else if(year === 2010) {
        //     this.setState({show2010: !this.state.show2010});
        // } else if(year === 2000) {
        //     this.setState({show2000: !this.state.show2000});
        // }
        this.setState({
            show2000: (year === 2000 ? !this.state.show2000 : this.state.show2000),
            show2010: (year === 2010 ? !this.state.show2010 : this.state.show2010),
            show2020: (year === 2020 ? !this.state.show2020 : this.state.show2020)
        }, () => {
            if(!this.state.show2020 && !this.state.show2010 && !this.state.show2000) {
                this.setState({supplyData: data});
                return;
            }
            const supplyData = data.map(item => {
                let returnItem = [item[0]];
                if(this.state.show2000) {
                    returnItem.push(item[1]);
                }
                if (this.state.show2010) {
                    returnItem.push(item[2]);
                }
                if (this.state.show2020) {
                    returnItem.push(item[3]);
                }
                return returnItem;
            })
            this.setState({supplyData: supplyData});
        });
    }

    render() {
        return (
            <div>
                <Chart
                width={'1000px'}
                height={'300px'}
                chartType="ColumnChart"
                loader={<div>Loading Chart</div>}
                data={this.state.supplyData}
                options={{
                    title: 'Population of Largest U.S. Cities',
                    chartArea: { width: '50%' },
                    isStacked: true,
                    hAxis: {
                    title: 'Total Population',
                    minValue: 0,
                    },
                    vAxis: {
                    title: 'City',
                    },
                }}
                // For tests
                rootProps={{ 'data-testid': '3' }}
                />
    
                <div className="switches">
                    <a className={this.state.show2000 ? 'included' : 'excluded'} onClick={()=> {this.modifyData(2000)}}>2000</a>
                    <a className={this.state.show2010 ? 'included' : 'excluded'} onClick={()=> {this.modifyData(2010)}}>2010</a>
                    <a className={this.state.show2020 ? 'included' : 'excluded'} onClick={()=> {this.modifyData(2020)}}>2020</a>
                </div>
            </div>
            
        );
    }
}

export default Bar;
