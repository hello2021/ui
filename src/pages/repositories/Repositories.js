import React, { Component } from 'react';
import { connect } from "react-redux";
import { getRepositories } from "../../store/actions";

class Repositories extends React.Component {
    constructor() {
        super();
    }

    componentDidMount() {
        this.props.getRepositories();
    }

    render() {
        return (
            <table>
                <thead>
                    <tr>
                        <th>Repo name</th>
                        <th>Language</th>
                        <th>Project</th>
                        <th>Owner</th>
                        <th>Branches</th>
                        <th>Current Branch</th>
                    </tr>
                </thead>
                <tbody>
                {this.props.repoList.map(repo => {
                    return (<tr className="user-info">
                        <td className="user-name">{repo.name}</td>
                        <td className="user-id">{repo.language}</td>
                        <td className="user-id">{repo.project}</td>
                        <td className="user-id">{repo.owner}</td>
                        <td className="user-id">
                            {
                                repo.branches.map(branch => (<div>{branch.name}</div>))
                            }
                        </td>
                        <td className="user-id">{repo.mainbranch}</td>
                    </tr>)
                })}
                </tbody>
            </table>
        )
    }
}

function mapStateToProps(state) {
    return {
        repoList: state.repositories
    };
}

export default connect(mapStateToProps, { getRepositories })(Repositories);