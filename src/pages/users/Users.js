import React, { Component } from 'react';

class Users extends React.Component {
    constructor() {
        super();
        this.state = {
            userList: [
                {
                    name: 'Virat Kohi',
                    emailId: 'virat@gmail.com'
                },
                {
                    name: 'Kevin Peterson',
                    emailId: 'kevin@gmail.com'
                },
                {
                    name: 'Chris Gayle',
                    emailId: 'gayle@gmail.com'
                },
                {
                    name: 'Bumrah',
                    emailId: 'bum@gmail.com'
                }
            ]
        }
    }
    render() {
        return (
            <table>
                <thead>
                    <tr>
                        <th>User Name</th>
                        <th>Email ID</th>
                    </tr>
                </thead>
                <tbody>
                {this.state.userList.map(user => {
                    return (<tr className="user-info">
                        <td className="user-name">{user.name}</td>
                        <td className="user-id">{user.emailId}</td>
                    </tr>)
                })}
                </tbody>
            </table>
        )
    }
}

export default Users;