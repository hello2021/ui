import React, { Component } from 'react';

import {
    Router,
    Switch,
    Route,
    Link,
    useRouteMatch,
    useParams,
    Redirect
} from "react-router-dom";

import { createBrowserHistory } from 'history';


import Users from "../users/Users.js"
import Repositories from "../repositories/Repositories.js"
import Dashboard from "../dashboard/Dashboard.js"

class Landing extends React.Component {

    history = createBrowserHistory();

    constructor() {
        super();
        this.state = { redirect: null };
    }

    loginClick = () => {
        this.history.push('/users')
    }

    render() {
        return (
            <div>
                
                <div className="container">
                    <Router history={this.history}>
                        <Switch>
                            <Route path="/users" component={Users} />
                            <Route path="/repositories" component={Repositories} />
                            <Route path="/dashboard" component={Dashboard} />
                        </Switch>
                    </Router>
                </div>


            </div>

        )
    }
}

export default Landing;