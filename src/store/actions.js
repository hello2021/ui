import { ADD_ARTICLE } from "./action-types";


export function getRepositories() {
    return { type: "DATA_REQUESTED", payload: { url: './mockdata/repositories.json', key: 'repositories' } };
}

export function getInsights() {
    return { type: "DATA_REQUESTED", payload: { url: './mockdata/insights.json', key: 'insights' } };
}
