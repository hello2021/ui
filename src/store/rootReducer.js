import {
    ADD_ARTICLE
} from "./action-types";

const initialState = {
    repositories: [],
    insights: [],
    authenticated: true,
    loggedUserDetails: {}
};

function rootReducer(state = initialState, action) {
    if (action.type === "DATA_LOADED") {
        const dataObj = {};
        dataObj[action.key] = state[action.key].concat(action.payload);
        return Object.assign({}, state, dataObj);
    }
    return state;
};

export default rootReducer;