**Installation**
1. Install node from https://nodejs.org/en/
2. Clone the repository
3. In the directory, run "npm install"

**Command to run the application locally**
Open command line and run "npm run start" and wait for few seconds, the application launches at localhost:3000 (default port)